#include <stdio.h>
#include <string.h>
// Proszę rozbudować program kalkulator wieloliczbowy z Lab 4 o możliwość zapisu/odczytu liczb do/z pliku (tekstowego):

// Zapis wszystkich wprowadzonych liczb do domyślnego pliku.
// Odczyt wszystkich liczb z domyślnego pliku.
// Zapis wszystkich wprowadzonych liczb do pliku o nazwie podanej przez użytkownika.
// Odczyt wszystkich liczb z pliku o nazwie podanej przez użytkownika.
// Dopisanie wszystkich wprowadzonych liczb do pliku o nazwie podanej przez użytkownika.
// Odczytanie wszystkich liczb z pliku i dopisanie ich do tablicy (z zachowaniem "starej" zawartości tablicy).
// Zamiast pytać użytkownika za każdym razem o nazwę pliku, pozwalamy mu podać domyślną nazwę pliku.

int tab[100];
int how_many_numbers;
int counter_numbers = 0;
char path_file_main[] = "plik.txt";
char path_user_file_main[] = "";
FILE *file;
char buffor[1000];
int flag=0;

void save_D_array_to_file(){
        if((file = fopen(path_file_main,"w"))!=NULL){
                for(int i =0;i<counter_numbers;i++){
                        fprintf(file,"%d ",tab[i]);
                }
                fclose(file);
        }else{
                printf("Bład odczytu pliku\n");
        }
}


void read_D_file_to_array(){
    char c;
    int i =0;
    int temp;
    if((file = fopen(path_file_main,"r"))!=NULL){
       
        while((fscanf(file, "%d ", &temp)>0)){

            
            tab[i] = temp;
            i++;
            
        }
        counter_numbers = i;

        fclose(file);
    }else{
        printf("Blad odczytu pliku\n");
    }
}

void save_array_to_file(){
    char tmp[300];
    if(flag == 0){
        printf("Podaj nowa sciezke pliku \n");
        scanf(" %[^\n]s",tmp);
        strcpy(path_user_file_main, tmp);
    }
        if((file = fopen(path_user_file_main,"w"))!=NULL){
                for(int i =0;i<counter_numbers;i++){
                        fprintf(file,"%d ",tab[i]);
                }
                fclose(file);
        }else{
                printf("Bład odczytu pliku\n");
        }
}

void read_file_to_array(){
    char c;
    int i =0;
    int temp;
    char tmp[300];
    if(flag == 0){
        printf("Podaj nowa sciezke pliku \n");
        scanf(" %[^\n]s",tmp);
        strcpy(path_user_file_main, tmp);
    }
    if((file = fopen(path_user_file_main,"r"))!=NULL){
       
        while((fscanf(file, "%d ", &temp)>0)){

            
            tab[i] = temp;
            i++;
            
        }
        counter_numbers = i;

        fclose(file);
    }else{
        printf("Blad odczytu pliku\n");
    }
}

void append_array_to_file(){ //przerobić
    char tmp[300];
    if(flag == 0){
        printf("Podaj nowa sciezke pliku \n");
        scanf(" %[^\n]s",tmp);
        strcpy(path_user_file_main, tmp);
    }
        if((file = fopen(path_user_file_main,"a"))!=NULL){
                for(int i =0;i<counter_numbers;i++){
                        fprintf(file,"%d ",tab[i]);
                }
                fclose(file);
        }else{
                printf("Bład odczytu pliku\n");
        }
}
// Odczytanie wszystkich liczb z pliku i dopisanie ich do tablicy (z zachowaniem "starej" zawartości tablicy).
// Zamiast pytać użytkownika za każdym razem o nazwę pliku, pozwalamy mu podać domyślną nazwę pliku.

void read_from_file_add_to_array(){

    char c;
    int i =0;
    int temp;
    
    if((file = fopen(path_file_main,"r"))!=NULL){
       
        while((fscanf(file, "%d ", &temp)>0)){

            
            tab[i+counter_numbers] = temp;
            i++;

            
        }
        counter_numbers= counter_numbers+i;

        fclose(file);
        for(int j = counter_numbers+1;j<100;j++ ){
        tab[j] = 0;
        }
    }
    else{
        printf("Blad odczytu pliku\n");
    }

}

void change_D_file_path(){
    char tmp[300];
    
    printf("Podaj nowa sciezke pliku \n");
    scanf(" %[^\n]s",tmp);
    strcpy(path_user_file_main, tmp);

    flag =1;
    
}

void input_numbers(){
    int temp = counter_numbers;
    for(int i=counter_numbers;i<how_many_numbers+temp; i++){
        scanf("%d",&tab[i]);
        counter_numbers++;
        
    }
    for(int i = counter_numbers+1;i<100;i++ ){
        tab[i] = 0;
    }
}

void print_numbers(){
    for(int i=0;i<counter_numbers; i++){
        printf("%d, ", tab[i]);
    }
}

void print_how_many_numbers(){
    printf("Liczb wprowadzonych zostało - %d",counter_numbers);
}

int calc_sum(){
    int result = 0;
    for(int i = 0;i<how_many_numbers; i++){
        result = result + tab[i];
    }
    return result;
}

float calc_arithmetic_avg(){
    
    return (float)calc_sum()/counter_numbers;
}

int search_number_inedex(int searched_number){
    for (int i = 0; i<counter_numbers; i++){
        if(tab[i] == searched_number){
            return i;
        }
    }
    return 404;
}

void change_number_in_tab(int number_before, int number_after){
    
    if(search_number_inedex(number_before) == 404){
        printf("Nie ma takiej liczby w tablicy ");
    }
    else{
        tab[search_number_inedex(number_before)] = number_after;
    }

}

void remove_lastNumber(){
    tab[counter_numbers-1] =0;
    counter_numbers--;

}

void remove_any_number(int nubmer_to_remove){
    
    if(search_number_inedex(nubmer_to_remove) == 404){
        printf("Nie ma takiej liczby w tablicy ");
    }
    else{
        for(int i = search_number_inedex(nubmer_to_remove);i<counter_numbers; i++){
            tab[i] = tab[i+1];
        }
        tab[counter_numbers] = 0;
        counter_numbers--;
    }
}

int min(){
    int min =tab[0];
    for(int i=0;i<counter_numbers;i++){
        if(tab[i]<=min){
            min = tab[i];
        }
    }
    return min;
}

int max(){
    int max =tab[0];
    for(int i=0;i<counter_numbers;i++){
        if(tab[i]>=max){
            max = tab[i]; 
        }
    }
    return max;
}

void stats(){
    int even_numbers = 0, odd_numbers = 0, positive_numbers = 0, negative_numbers = 0;
    for(int i=0;i<counter_numbers;i++){
        
        if(tab[i] % 2 == 0){
            even_numbers++;
        }
        else{
            odd_numbers++;
        }

        if(tab[i] > 0){
            positive_numbers++;
        }else{
            negative_numbers++;
        }
    }
    printf("\n");
    printf("Parzystych liczb jest: %d \n",even_numbers);
    printf("Nieparzystych liczb jest: %d \n",odd_numbers);
    printf("Dodatnich liczb jest: %d \n",positive_numbers);
    printf("Ujemnych liczb jest: %d \n",negative_numbers);
}
// Zapis wszystkich wprowadzonych liczb do domyślnego pliku.
// Odczyt wszystkich liczb z domyślnego pliku.
// Zapis wszystkich wprowadzonych liczb do pliku o nazwie podanej przez użytkownika.
// Odczyt wszystkich liczb z pliku o nazwie podanej przez użytkownika.
// Dopisanie wszystkich wprowadzonych liczb do pliku o nazwie podanej przez użytkownika.
// Odczytanie wszystkich liczb z pliku i dopisanie ich do tablicy (z zachowaniem "starej" zawartości tablicy).
// Zamiast pytać użytkownika za każdym razem o nazwę pliku, pozwalamy mu podać domyślną nazwę pliku.

int main(){
int select;
int number_before, number_after;
int number_to_remove;
    do{
        printf("\n");
        printf("Wybierz dzialanie\n\n");

        printf("1 - Wprowadzenie liczb max 100\n");
        printf("2 - Wyswietlenie liczb \n");
        printf("3 - Ile jest liczb wprowadzonych\n");
        printf("4 - Obliczanie sumy\n");
        printf("5 - Obliczanie średniej \n");
        printf("6 - Zmiana liczby   \n");
        printf("7 - Usunięcie Ostatniej liczby z tablicy  \n");
        printf("8 - Usuniecie dowolnej liczby \n");
        printf("9 - Min Max\n");
        printf("10 - Wyświetlenie statystyki (ile parzystych, ile nieparzystych, ile dodatnich, ile ujemnych) \n");
        printf("11 - Zapisz do domyslnego pliku \n");
        printf("12 - Odczyt wszystkich liczb z domyślnego pliku. \n");
        printf("13 - Zapis wszystkich wprowadzonych liczb do pliku o nazwie podanej przez użytkownika. \n");
        printf("14 - Odczyt wszystkich liczb z pliku o nazwie podanej przez użytkownika. \n");
        printf("15 - Dopisanie wszystkich wprowadzonych liczb do pliku o nazwie podanej przez użytkownika.\n");
        printf("16 - Odczytanie wszystkich liczb z pliku i dopisanie ich do tablicy (z zachowaniem starej zawartości tablicy) \n");
        printf("17 - Zamiast pytać użytkownika za każdym razem o nazwę pliku, pozwalamy mu podać domyślną nazwę pliku.\n");



        printf("0 - Koniec\n\n");

        scanf("%d",&select);

        printf("\n");
        
        switch(select){
            case 1:
                printf("Podaj ile liczb w tablicy \n");
                scanf("%d", &how_many_numbers);
                printf("\n");
                input_numbers();
                break;
            case 2:
                print_numbers();
                break;
            case 3:
                print_how_many_numbers();
                break;
            case 4:
                printf("Suma Wynosi - %d",calc_sum());
                break;
            case 5:
                printf("Średnia arytmetyczna wynosi %f",calc_arithmetic_avg());
                break;
            case 6:
               
                print_numbers();
                printf("\n");
                printf("Podaj liczbe jaką chcesz zmienic \n");
                scanf("%d",&number_before);
                printf("Podaj liczbe na jaka chcesz zmienić \n");
                scanf("%d", &number_after);
                change_number_in_tab(number_before,number_after);
                printf("\n");
                printf("Twoje liczby po zmianie \n");
                print_numbers();
                break;
            case 7:
                print_numbers();
                printf("\n");
                remove_lastNumber();
                print_numbers();
                printf("\n");
                break;
            case 8:
                
                printf("\n");
                printf("Podaj cyfre do usuniecia ");
                scanf("%d", &number_to_remove);
                remove_any_number(number_to_remove);
                printf("\n");
                print_numbers();
                break;
            case 9:
                printf("\n");
                printf("Twoje Max to: %d \n Twoje Min to: %d",max(),min());
                break;
            case 10:
                stats();
                break;
            case 11:
                save_D_array_to_file();
                break;
            case 12:
                read_D_file_to_array();
                break;
            case 13:
                save_array_to_file();
                break;
            case 14:
                read_file_to_array();
                break;
            case 15:
                append_array_to_file();
                break;
            case 16:
                read_from_file_add_to_array();
                break;
            case 17:
                change_D_file_path();
                break;

        }

    }while(select !=0);

    return 0;
}
